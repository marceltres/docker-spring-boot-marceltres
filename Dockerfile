FROM openjdk:12
ADD target/docker-spring-boot-marceltres.jar docker-spring-boot-marceltres.jar
EXPOSE 8085
ENTRYPOINT ["java", "-jar", "docker-spring-boot-marceltres.jar"]
